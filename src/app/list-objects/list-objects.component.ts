import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-list-objects',
  templateUrl: './list-objects.component.html',
  styleUrls: ['./list-objects.component.css']
})
export class ListObjectsComponent implements OnInit {
  @HostBinding('attr.class') cssClass = 'col-md-8';
  nombre: string;
  listobjs: string[];
  constructor() {
    this.nombre = 'Lista de objetos';
    this.listobjs = [];
   }

  ngOnInit() {
  }

  guardar(element: string) {
    this.listobjs.push(element);
    console.log('add item: ' + this.listobjs);
    return false;
  }

}
